local sensorInfo = {
	name = "GetSafeGrid",
	desc = "Grid of safe positions acording to given enemy units.",
	author = "novotnyt",
	date = "2018-05-29",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local spEcho = Spring.Echo
local spGetUnitPosition = Spring.GetUnitPosition
local spGetHeight = Spring.GetGroundHeight

return function(range)
  JUMP = 50
  base = {}
  for x = math.ceil((3800-range)/JUMP), math.floor((3800+range)/JUMP) do
    base[x] = {}
    for z =  math.ceil((750-range)/JUMP),math.floor((750+range)/JUMP) do
      base[x][z] = 1
    end
  end
  return base
end