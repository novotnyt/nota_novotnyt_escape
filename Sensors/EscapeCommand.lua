local sensorInfo = {
	name = "EscapeCommand",
	desc = "Computes safe paths for all selected units to given units.",
	author = "novotnyt",
	date = "2018-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local spEcho = Spring.Echo
local spCommand = Spring.GiveOrderToUnit
local spGetUnitPosition = Spring.GetUnitPosition
local spGetHeight = Spring.GetGroundHeight

-- @description return current wind statistics
return function(pathCommands)    
  for i, cmd in ipairs(pathCommands) do 
    spCommand(cmd[1], CMD.STOP, {}, {})
    for j = 1, #(cmd[3])-1 do
      wayPoint = cmd[3][j]
      spCommand(cmd[1], CMD.MOVE, {wayPoint[1], spGetHeight(wayPoint[1], wayPoint[2]), wayPoint[2]}, {"shift"})
    end
    if cmd[2] < 0 then
      endPoint = cmd[3][#(cmd[3])]
      spCommand(cmd[1], CMD.TIMEWAIT, {2000}, {"shift"})
      spCommand(cmd[1], CMD.UNLOAD_UNIT, {endPoint[1], spGetHeight(endPoint[1], endPoint[2]), endPoint[2]}, {"shift"})
      spCommand(cmd[1], CMD.MOVE, {3800, spGetHeight(3800, 100), 100}, {"shift"})
    else
      spCommand(cmd[1], CMD.LOAD_UNITS, {cmd[2]}, {"shift"})
    end
  end
  return RUNNING
end