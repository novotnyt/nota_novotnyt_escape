local sensorInfo = {
	name = "GetSafeGrid",
	desc = "Grid of safe positions acording to given enemy units.",
	author = "novotnyt",
	date = "2018-05-29",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local spEcho = Spring.Echo
local spGetUnitPosition = Spring.GetUnitPosition
local spGetHeight = Spring.GetGroundHeight

return function(forbiddenPlaces)
  JUMP = 50
  HEIGHT_LIMIT = 300
	width = Game.mapSizeX
  height = Game.mapSizeZ
  spEcho("Map size:", width, height)  
  
  --initialize discretized world map
  worldMap = {}  
  for w = 1, width/JUMP do
    worldMap[w] = {}
    for h = 1, height/JUMP do      
      worldMap[w][h] = 0 
    end
  end
  
  
  local function getDistance(pos1, pos2)   
    return math.sqrt((pos1.x - pos2.x)^2 + (pos1.z - pos2.z)^2)
  end 
  --exclude forbidden areas
  for i, place in ipairs(forbiddenPlaces) do
    local left = math.max(math.ceil((place[1].x - place[2])/JUMP), 1)
    local right = math.min(math.floor((place[1].x + place[2])/JUMP), width/JUMP)
    local top = math.max(math.ceil((place[1].z - place[2])/JUMP), 1)
    local bottom = math.min(math.floor((place[1].z + place[2])/JUMP), height/JUMP)
    
    for x = left, right do
      for z = top, bottom do
        if getDistance(place[1], Vec3(x*JUMP, 0, z*JUMP)) < place[2] then
          ok = true
          for dX = -2, 2 do
            for dZ = -2, 2 do  --check height constraints         
              if spGetHeight(place[1].x, place[1].z) - spGetHeight((x+dX)*JUMP, (z+dZ)*JUMP) < HEIGHT_LIMIT then
                ok = false
                break
              end
            end
          end
          if not ok then
            worldMap[x][z] = 1
          end
        end
      end
    end
  end
  return worldMap
end