local sensorInfo = {
	name = "GetPickableUnitIDs",
	desc = "Returns all units which should be saved.",
	author = "novotnyt",
	date = "2018-05-29",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local spEcho = Spring.Echo
local spGetTeamUnits = Spring.GetTeamUnits
local spGetUnitDefID = Spring.GetUnitDefID
local spGetLocalTeamID = Spring.GetLocalTeamID
local spGetUnitTransporter = Spring.GetUnitTransporter
-- @description return current wind statistics
return function()       
  local unitList = {}
  for i, unit in ipairs(spGetTeamUnits(spGetLocalTeamID())) do
    if spGetUnitTransporter(unit) == nil then
      unitDef = spGetUnitDefID(unit)
      --IDS: 34=BoD, 108, 78, 38=BD, 127
      wantedIDs = {34, 108, 78, 38, 127}
      --add unit if that ID is in list
      for _, WID in ipairs(wantedIDs) do
        if WID == unitDef then
          unitList[#unitList+1] = unit  
        end
      end
    end
  end
  return unitList
end