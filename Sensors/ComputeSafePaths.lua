local sensorInfo = {
	name = "ComputeSafePaths",
	desc = "Computes safe paths for all selected units to given units.",
	author = "novotnyt",
	date = "2018-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local spEcho = Spring.Echo
local spCommand = Spring.GiveOrderToUnit
local spGetUnitPosition = Spring.GetUnitPosition
local spIsTransporting = Spring.GetUnitIsTransporting

-- @description return current wind statistics
return function(worldMap, savedUnits, basePoints)    
  JUMP = 50
  width = #worldMap
  height = #(worldMap[1])
  
  --utility functions
  local function uPos(x, z)
    return x*height + z
  end
  
  local function gridPos(unitID)
    posX, posY, posZ = spGetUnitPosition(unitID)
    if posX ~= nil then    
      gX = math.round(posX / JUMP)
      gZ = math.round(posZ / JUMP)
      return math.min(width, math.max(1, gX)), math.min(height, math.max(1, gZ))  
    end
  end
  
  local function getDirList(dir)
    if dir < 1 then
      dir = 1
    end   
    if dir%2 == 1 then  
      return {dir, (dir+1)%8+1, (dir+5)%8+1, dir%8+1, (dir+6)%8+1, (dir+2)%8+1, (dir+4)%8+1}
    else
      return {dir%8+1, (dir+6)%8+1, (dir+2)%8+1, (dir+4)%8+1, dir, (dir+1)%8+1, (dir+5)%8+1}
    end
  end  
  
  local function diffDir(dir)
    seq = {0,1,1,1,0,-1,-1,-1}
    if dir == 0 then
      return 0, 0
    else
      return seq[dir], seq[(dir+5)%8+1]
    end
  end
  
  local function disableBaseArea(gridX, gridZ)
      basePoints[gridX][gridZ] = nil
    for i = 1, 8 do
      dX, dZ = diffDir(i)
      if basePoints[gridX + dX] ~= nil then 
        basePoints[gridX + dX][gridZ + dZ] = nil
      end
    end
  end
  
  --find grid nodes of savedUnits 
  unitPositions = {}
  unsaved = 0
  for i, unit in ipairs(savedUnits) do  
    gridX, gridZ = gridPos(unit)
    if gridX ~= nil then
      if basePoints[gridX] ~= nil and basePoints[gridX][gridZ] ~= nil then
        --there is a unit in base already, ignore its grid point
        disableBaseArea(gridX, gridZ)
      else
        unsaved = unsaved + 1
        --allow to enter dangerous zone a bit
        worldMap[gridX][gridZ] = 0
        if unitPositions[uPos(gridX, gridZ)] == nil then
          unitPositions[uPos(gridX, gridZ)] = {unit}
          
        else
          unitPositions[uPos(gridX, gridZ)][#unitPositions[uPos(gridX, gridZ)] + 1] = unit
        end
      end    
    end
  end 
    
  --make BFS from selected units to find closest ally
  paths = {}
  for unit = 1, #units do
    if unsaved == 0 then --do not continue when there is no unit left
      break
    end
    done = false
    baseX, baseZ = gridPos(units[unit]) 
    returning = (#spIsTransporting(units[unit]) > 0)
    visited = {}   -- hashtable of visited grid nodes with value equal to entry direction (N=1, NW=8)
    openVertices = {{baseX, baseZ, 0}}
    quePos = 1
    while quePos <= #openVertices do
      vertex = openVertices[quePos]
      curX = vertex[1]
      curZ = vertex[2]
      dir = vertex[3]
      quePos = quePos + 1
      if visited[uPos(curX, curZ)] == nil then
        --new node, we expand it
        visited[uPos(curX, curZ)] = dir

        --check, whether we found the target
        if returning and basePoints[curX] ~= nil and basePoints[curX][curZ] ~= nil then
          spEcho("Done1")
          done = true
          targetUnit = -1
          disableBaseArea(curX, curZ) --disable that place for others
        elseif not returning and unitPositions[uPos(curX, curZ)] ~= nil then
          spEcho("Done2")
          done = true
          unsaved = unsaved - 1
          num = #(unitPositions[uPos(curX, curZ)])
          targetUnit = unitPositions[uPos(curX, curZ)][num]        
          if num > 1 then            
            unitPositions[uPos(curX, curZ)][num] = nil
          else
            unitPositions[uPos(curX, curZ)] = nil
          end  
        end
        
        if done then  
          --create waypoint description of the map
          tempWayPoints = {{curX*JUMP, curZ*JUMP}}
          while true do
            diffX, diffZ = diffDir(dir)
            prevX = curX - diffX
            prevZ = curZ - diffZ
            prevDir = visited[uPos(prevX, prevZ)]
            if prevDir <= 0 then
              break
            end
            if prevDir ~= dir then
              tempWayPoints[#tempWayPoints + 1] = {prevX*JUMP, prevZ*JUMP}          
            end
            curX = prevX
            curZ = prevZ
            dir = prevDir            
          end
          
          wayPoints = {}
          for j = 1, #tempWayPoints do
            wayPoints[j] = tempWayPoints[#tempWayPoints + 1 - j]
          end
          paths[#paths + 1] = {units[unit], targetUnit, wayPoints}
          break
        end
       
        newDirList = getDirList(dir)
        for _, nextDir in ipairs(newDirList) do
          diffX, diffZ = diffDir(nextDir) 
          newX = curX + diffX
          newZ = curZ + diffZ
          --if newX >= 1 and newX <= width and 
          --   newZ >= 1 and newZ <= height and
          if worldMap[newX] ~= nil and worldMap[newX][newZ] == 0 and visited[uPos(newX, newZ)] == nil then --automatically solves range check
            openVertices[#openVertices + 1] = {newX, newZ, nextDir}
          end
        end
      end
    end  
  end
  return paths
end