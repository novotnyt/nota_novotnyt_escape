local sensorInfo = {
	name = "GetEnemyPos",
	desc = "Returns pairs of positions of enemies with forbidden ranges.",
	author = "novotnyt",
	date = "2018-05-29",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local spEcho = Spring.Echo
local spGetTeamUnits = Spring.GetTeamUnits
local spGetLocalTeamID = Spring.GetLocalTeamID
local spGetTeamList  = Spring.GetTeamList
local spGetUnitPosition = Spring.GetUnitPosition

-- @description return current wind statistics
return function() 
  local enemyList = {}     
  myTeam = spGetLocalTeamID() 
  for i, team in ipairs(spGetTeamList()) do
    if team ~= myTeam then
      for j, unit in ipairs(spGetTeamUnits(team)) do
        x,y,z = spGetUnitPosition(unit)
        enemyList[#enemyList+1] = {Vec3(x,y,z), 1200} --TODO ranges
      end
    end
  end
  return enemyList
end